var express = require("express"),
    controllers = require("./Controllers"),
    configs = require("./Configs"),
    http = require("http"),
    io = require("socket.io");

var app = express();
var server = http.Server(app);
var ioApp = io(server);

var options = {
    dirname: __dirname,
    port: process.env.PORT || 3244,
	basePath: process.env.deployPath || ""
};

// config the app
configs(app, options, function () {
    // log if something fails
    console.error("failed to start the app");
}, function () {
    // init the controlelrs
    controllers(app, ioApp, options, function () {
        // run the app if everything was ok
        server.listen(options.port);
        console.log("Listening on port", options.port);
    });
});