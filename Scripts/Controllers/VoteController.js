(function () {
    "use strict";
	
	var toastr = window.toastr || {};
    
    var MainController = function ($routeParams, $scope, $http) {
        var _ = require("./../../bower_components/underscore/underscore-min");
        var io = require("./../../node_modules/socket.io-client");
        var socket = io();
        
        $scope.votes = [];
        
		socket.on("connect", function () {
			toastr.success("Successfully connected with the server.")
			$http.get("/votes").success(function (data) {
				$scope.votes.length = 0;
				data.forEach(function (vote) {
					$scope.votes.push(vote);
				})
			});
		});
		
		socket.on("connect_error", function () {
			toastr.warning("Connection with the server lost. Please reload the page");
		});
		
        socket.on("update vote", function (data) {
            var index = _.findIndex($scope.votes, function (v) {
                return v.id === data.id;
            });
            if (index < 0) return;
            
            $scope.votes[index].count = data.count;
            $scope.$apply();
        });
        
        $scope.addVote = function (voteId) {
            socket.emit("vote", voteId);
        }
    };
    
    var register = function (app) {
        
        app.controller("VoteController", MainController);
        
    };
    
    module.exports = { register: register };
})();
