(function () {
    
    var register = function(app) {

        require("./MainController").register(app);
        require("./VoteController").register(app);
        
    }

    module.exports = {
        register: register
    };

})();