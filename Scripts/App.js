(function(){
    "use strict";
    
    var angular = require("../bower_components/angularjs"),
        angularRoute = require("../bower_components/angular-route"),
        controllers = require("./Controllers"),
        directives = require("./Directives"),
        configs = require("./Configs");
    
    var app = angular.module("VoteR", ["ngRoute"]);

    controllers.register(app);
    directives.register(app);
    configs.config(app);

    app.constant("initUrl", "/");
    app.constant("baseHref", "/");
})();