(function() {
    "use strict";
    
    var register = function(app) {

        require("./VoteDirective").register(app);
        
    }

    module.exports = {
        register: register
    }

})();