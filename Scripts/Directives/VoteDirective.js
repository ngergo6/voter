(function() {

    var register = function(app) {

        app.directive("vote", function () {
            return {
                restrict: "E",
                templateUrl: "/directives/Vote.html"
            };
        });
        
    };

    module.exports = {
        register: register
    };

})();