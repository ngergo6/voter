(function () {
    
    var config = function (app) {
        
        require("./RouteConfig").config(app);
    }
    
    module.exports = {
        config: config
    }

})();