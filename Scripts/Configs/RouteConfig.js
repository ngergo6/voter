(function () {
    
    var config = function (app) {
        
        app.config(function ($routeProvider, $locationProvider) {
            $locationProvider.hashPrefix("").html5Mode(false);
            
            $routeProvider
            .when("/", {
                templateUrl: "templates/Home.html"
            })
            .otherwise({
                redirectTo: "/"
            });
        });
    };
    
    module.exports = {
        config: config
    }
})();