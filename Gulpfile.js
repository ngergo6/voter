var gulp = require("gulp"),
    gutil = require("gulp-util"),
    sourcemaps = require("gulp-sourcemaps"),
    source = require("vinyl-source-stream"),
    buffer = require("vinyl-buffer"),
    watchify = require("watchify"),
    browserify = require("browserify"),
    brfs = require("brfs");

var bundler = watchify(browserify("./Scripts/App.js", watchify.args));

bundler.transform("brfs");

gulp.task("js", bundle);
bundler.on("update", bundle);
bundler.on("log", gutil.log);

function bundle () {
    return bundler.bundle()
        .on("error", gutil.log.bind(gutil, "Browserify error"))
        .pipe(source("bundle.js"))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(sourcemaps.write("./"))
        .pipe(gulp.dest("./Scripts/"));
}