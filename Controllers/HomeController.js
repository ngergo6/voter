var homeController = function (app, io, options) {
    "use strict";
    
    app.get("/", function (req, res) {
        res.render("Home", {});
    });
    
}

module.exports = homeController;
