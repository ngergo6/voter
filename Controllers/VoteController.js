var voteController = function (app, io, options) {
    "use strict";
    
    var _ = require("underscore");
    
    var votes = [
        { id: 1, name: "Vote me please!", count: 0 },
        { id: 2, name: "This is the best vote", count: 0 },
        { id: 3, name: "Do not vote for me", count: 0 },
        { id: 4, name: "Hello vote", count: 0 },
        { id: 5, name: "FREE COOKIES HERE!!", count: 0 }
    ];
    
    app.post("/vote/:id", function (req, res) {
        var voteId = req.params.id;
        
		var index = _.findIndex(votes, function(vote){
            return vote.id == voteId;
        });
        if (index < 0) return;

        votes[index].count += 1;
        io.emit("update vote", votes[index]);
    });
    
    app.get("/votes", function (req, res) {
        res.send(votes);
    });
    
    io.on('connection', function(socket){
        console.log("a user connected");
        
        socket.on("disconnect", function () {
            console.log("a user has disconnected");
        });
        
        socket.on("vote", function (voteId) {
            var index = _.findIndex(votes, function(vote){
                return vote.id === voteId;
            });
            if (index < 0) return;
            
            votes[index].count += 1;
            io.emit("update vote", votes[index]);
        });
        
    })
}

module.exports = voteController;
