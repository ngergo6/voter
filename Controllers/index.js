var controllersList = ["HomeController", "VoteController"];

var initControllers = function (app, io, options, callback) {
    "use strict";
    
    console.log("Register controllers:");
    
    controllersList.forEach(function (controllerName) {
        var controllerModule = require("./" + controllerName);
        console.log("...registered:", controllerName);
        controllerModule(app, io, options)
    });
    
    if (callback) callback();
};

module.exports = initControllers;