var express = require("express"),
    path = require("path");

var config = function (app, options, err, callback) {
	
    var registerPath = function(virtual, actual){
        app.use(virtual, express.static(path.join(options.dirname, actual)));
    };
    
    // set static routes
    registerPath("/public", "/bower_components");
    registerPath("/lib", "/Scripts");
    registerPath("/css", "/Contents/css");
    registerPath("/img", "/Contents/img");
    registerPath("/templates", "/Views/Templates");
    registerPath("/directives", "/Views/Directives");
    
    if (callback) callback();
};

module.exports = config;