var express = require("express"),
    handlebars = require("express-handlebars");

var config = function (app, options, err, callback) {
    
    // set routes
    app.engine(".html", handlebars({ defaultLayout: "Main", extname: ".html" }));
    app.set("view engine", ".html");
    
    if (callback) callback();
}

module.exports = config;