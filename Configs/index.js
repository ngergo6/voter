var $ = require("jquery-deferred");

var configsList = ["ViewEngineConfig", "RouteConfig"];

var runConfigs = function (app, options, err, callback) {
    "use strict";
    
    console.log("Running configs:");
    
    var defs = [];
    
    configsList.forEach(function (configName) {
        var def = $.Deferred();
        
        var configModule = require("./" + configName);
        
        configModule(app, options, function(){
            // on error
            def.reject();
            console.error("...config failed:", configName);
        }, function(){
            def.resolve();
            console.log("...config done:", configName);
        });
        
    });
    
    $.when.apply($, defs).done(function(){
        if (callback) callback();
    }).fail(function(){
        if (err) err();
    });
};

module.exports = runConfigs;