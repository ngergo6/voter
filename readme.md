Prerequisites:

1. Latest version of NodeJS.
2. Bower installed via NPM ('npm install bower -g').
3. Browserify installed via NPM ('npm install browserify -g').  
Optional: Gulp installed via NPM ('npm install gulp -g').

Steps to run:

1. Install NPM packages ('npm install').
2. Install Bower packages ('bower install').
3. Bundle up the client-side JavaScript modules ('gulp js' for development, 'browserify Scripts/app.js > Scripts/bundle.js').  
Optional: set a port to run on. 'set deployPath=PORTNUMBER' for windows, '$deployPath=PORTNUMBER' for unix and mac.
4. Run the app with the 'npm start' command.